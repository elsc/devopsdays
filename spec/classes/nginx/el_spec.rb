require 'spec_helper'

describe 'devopsdays::nginx::el' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) do
        "
        class { 'devopsdays':
          fix_cgi            => false,
          php_extensions     => {},
          fpm_listen_owner => 'foo',
          fpm_listen_group => 'bar',
          fpm_user           => 'foobar',
          fpm_group          => 'baz',
          fpm_skt_dir        => '/foo/bar/baz',
        }
        "
      end

      if os_facts[:os]['name'] == 'Ubuntu'
        it { is_expected.not_to compile }
      else
        it { is_expected.to compile }
        it { is_expected.to contain_file('/usr/share/nginx/html/info.php').with_ensure('file') }
        it {
          is_expected.to contain_nginx__resource__server('example.com').with(
            'ensure'               => 'present',
            'listen_port'          => 80,
            'www_root'             => '/usr/share/nginx/html',
            'use_default_location' => false,
            'index_files'          => [
              'index.php',
              'index.html',
              'index.htm',
            ],
            'server_name' => ['example.com'],
          )
        }
        it { is_expected.to contain_nginx__resource__location('php_files_location').with(
          'ensure'        => 'present',
          'server'        => 'example.com',
          'location'      => '~ \.php$',
          'fastcgi'       => 'unix:/var/run/php-fpm/php-fpm.sock',
          'fastcgi_index' => 'index.php',
          'fastcgi_param' => {
            'SCRIPT_FILENAME' => '$document_root$fastcgi_script_name',
          },
          priority      => 501,
			)
		}
		it { is_expected.to contain_nginx__resource__location('php_files_location').with_server('example.com') }
        it { is_expected.to contain_nginx__resource__location('50x_errors').with_server('example.com') }
      end
    end
  end
end
