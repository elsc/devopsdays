require 'spec_helper'

describe 'devopsdays::firewall' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:pre_condition) do
        "
        class { 'devopsdays':
          fix_cgi            => false,
          php_extensions     => {},
          fpm_listen_owner => 'foo',
          fpm_listen_group => 'bar',
          fpm_user           => 'foobar',
          fpm_group          => 'baz',
          fpm_skt_dir        => '/foo/bar/baz',
        }
        "
      end

      it { is_expected.to compile }
      it { is_expected.to contain_class('devopsdays::firewall') }
      it { is_expected.to contain_class('firewall') }
      it {
        is_expected.to contain_firewall('100 allow http traffic').with(
          'ensure' => 'present',
          'dport'  => ['80', '443'],
          'proto'  => 'tcp',
          'action' => 'accept',
        )
      }
    end
  end
end
